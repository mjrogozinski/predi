#pragma once
#include <vector>
#include <cstdint>

namespace predi
{

using Trades = std::vector<std::int32_t>;
using TradesDerivative = std::vector<std::int32_t>;
using Prediction = float;

Prediction predict(Trades const&, TradesDerivative const&);

}
